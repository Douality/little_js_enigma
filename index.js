var currentQuestion = 0;
var open_res = false;
var code_secret = [0, 0, 0, 0];
//memory enigma grammar vocabulary

function calcCodeSecret() {
    var memory = 0;
    var enigma = 0;
    var grammar = 0;
    var vocabulary = 0;

    for(var i = 0; i < questions.length; i ++) {
        switch(questions[i].groups) {
            case "Vocabulary":
                vocabulary += questions[i].rep;
                code_secret[3]++;
                break;
            case "Grammar":
                grammar += questions[i].rep;
                code_secret[2]++;
                break;
            case "Memory":
                memory += questions[i].rep;
                code_secret[0]++;
                break;
            case "Enigma":
                enigma += questions[i].rep;
                code_secret[1]++;
                break;
        }
    }
    //
    code_secret[0] = Math.floor( memory / code_secret[0] );
    code_secret[1] = Math.floor( enigma / code_secret[1] );
    code_secret[2] = Math.floor( grammar / code_secret[2] );
    code_secret[3] = Math.floor( vocabulary / code_secret[3] );

    console.log(code_secret);
}

function addEvent() {

    $(".book").click(function (e) { 
        StartQuestion();
    });

    $(".safebox").click(function (e) { 
        StartCodePopup();
    });

    $(".code-block").change(function (e) { 
        var codes = $(".code-block");
        var match = 0;
        var notnull = 0;
        for(var i = 0; i < codes.length; i ++) {
            if(codes.eq(i).val() != "") {
                notnull ++;
            }
            if(parseInt(codes.eq(i).val()) == code_secret[i]) {
                match++;
            }
        }

        if(notnull == code_secret.length) {
            if(match == code_secret.length) {
                alert("Password correct.");
            } else {
                alert("Password incorrect.");
                //
                codes.val("");
            }
        }

    })

    $(".question-popup .answer").click(function(e) {
        if(open_res == false) {
            $(".question-popup .answer").css("color", "#0029ff");
            $("#group-" + questions[currentQuestion].groups.toLowerCase()).append("   "+ questions[currentQuestion].rep);
            open_res = true;
            currentQuestion++;
        }
    });

    $(".question-popup .hint").click(function(e) {
        $(".question-popup .hint span").css("color", "#201ad6");
    })

    $(".question-popup .close-btn").click(function(e) {
        $(".question-popup").fadeOut(300);
    });

    $(".code-popup .close-btn").click(function(e) {
        $(".code-popup").fadeOut(300);
    });
}

function StartQuestion() {
    if(currentQuestion < questions.length) {
        var ques = questions[currentQuestion];
        open_res = false;
        $(".question-popup .group").html(ques.groups.toUpperCase());
        $(".question-popup .question").html("<span style='color:#e96015;'>Question: </span>"+ ques.ques);
        $(".question-popup .hint span").html(ques.hint);
        $(".question-popup .hint span").css("color", "white");
        //delete old response
        $(".question-popup .answer").html(ques.rep);
        $(".question-popup .answer").css("transform", "rotate3d(0, 1, 0, 0deg)");
        $(".question-popup .answer").css("color", "#a5ffa4");
        
        //
        $(".question-popup").fadeIn(300);
    } else {
        $(".question-popup .group").css("display", "none");
        $(".question-popup .question").html("You had finished all the questions.");
        $(".question-popup .answer").css("display", "none");
        //
        $(".question-popup").fadeIn(300);
    }
}

function StartCodePopup() {
    $(".code-popup").fadeIn(300);
}

$(document).ready(function () {
    calcCodeSecret();
    addEvent();    
});

