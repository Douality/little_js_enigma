# Jeu du Pendu en JavaScript

Ce petit projet consiste en une implémentation simple du jeu décodeur en JavaScript. Il s'agit d'un jeu de devinette où le joueur doit deviner un code caché en proposant des combinaisons.

## Comment jouer

1. Ouvrez le fichier `index.html` dans un navigateur web pour lancer le jeu.
2. Un mot secret est choisi au hasard, et ses lettres sont représentées par des espaces vides.
3. Vous pouvez deviner des lettres en cliquant sur les boutons correspondants ou en les tapant au clavier.
4. Si la lettre est correcte, elle sera affichée dans le mot secret.
6. Le jeu se termine lorsque vous devinez le mot.

## Exigences

Ce jeu est implémenté en utilisant uniquement HTML, CSS et JavaScript. Aucune dépendance externe n'est nécessaire.

## Améliorations possibles

- Ajouter un système de scores.
- Améliorer l'interface utilisateur avec des graphiques plus élaborés.
- Implémenter une fonctionnalité de sélection de catégories de mots.
- Gérer les niveaux de difficulté.

## Pourquoi choisir JavaScript avec WebGL ou C++ avec OpenGL ?

Ce petit jeu en JavaScript montre les bases de ce que vous pouvez faire en termes de jeux dans un navigateur web en utilisant des technologies web standard. Si vous êtes nouveau dans le développement de jeux, c'est un bon point de départ pour comprendre les concepts fondamentaux.

Cependant, si vous cherchez à créer des jeux plus complexes avec des graphiques 3D, des performances optimales et un contrôle plus fin sur le matériel, vous pourriez envisager d'apprendre WebGL avec JavaScript ou OpenGL avec C++. Ces technologies vous permettront de créer des jeux plus avancés, mais elles nécessitent également une courbe d'apprentissage plus raide.

Ce petit jeu est plutôt là comme un pré-test à savoir si le js pour le jeu , ne vous plait déjà pas et dans ce cas C++ OpenGl sera une meilleure solution.
