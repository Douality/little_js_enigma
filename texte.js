
var questions = [
    {
        groups : "Enigma",
        hint : "Value of the array Something[] = {month, month, month}",
        ques : "Winter[0], autumn[0], spring[0], summer[0], Autumn[0], winter[0], spring[0], One word is missing, which one?",
        rep : 6,
        anw : "Something[0]"
    },

    {
        groups : "Enigma",
        hint : "std::sqrt(+-*). -10x ",
        ques : "Find the squareroot of ((two add tree minus one) power two) concatenate two minus one, then forget two times ten and one more to go",
        anw : 2,
        rep : 31
    },
    
    {
        groups : "Enigma",
        hint : "1 + 2 = 3, 3 + X = 8, X + 8 = 13",
        ques : "I have a sequence of number: 1,2,3,X,8,13. Find X?",
        anw : "Begin is the end and end not the beginning",
        rep : 5
    },
    {
        groups : "Grammar",
        hint : "Past, Present, Future | Simple, Continuous, Perfect, Perfect continuous",
        ques : "In passive form, how many tenses are there in English?",
        anw : "",
        rep : 12
    },
    {
        groups : "Grammar",
        hint : "Remember the position of adj in sentence",
        ques : "The young work fast but carelessly, on the other hand, the old is slower but more patient. \n How many adjective are there in this sentence?",
        anw : "",
        rep : 3
    },

    {
        groups : "Grammar",
        hint : "Pay attention on the letter 's'",
        ques : "I'ds likes tos bes ables tos helps yous, howsever, Is stills needs tos thinks abouts yours circumstances , Some errors have been puts into this sentences, how many ? ",
        anw : 18,
        rep : 18
    },

    {
        groups : "Vocabulary",
        hint : "Just count + existance number",
        ques : "greeting and lightenth, differences ?",
        anw : "Just count, you certainly have to begin with something...",
        rep : 10
    },

    {
        groups : "Vocabulary",
        hint : "I have been playing at Fortnite .... power.",
        ques : "J’ai joué à Fortnite quand ma mère a coupé la lumière. Give me english traduction then you got the number --5",
        anw : 6,
        rep : 6
    },

    {
        groups : "Vocabulary",
        hint : " Other + Pleased + Ha-Delig-Satis-Gratified with her new music content",
        ques : "The teacher was pleased with the students' test results. Give me the number of synonyms of the word 'pleased' in this sentance",
        anw : "The first question on a control is always vocabulary",
        rep : 21
    },

    {
        groups : "Memory",
        hint : "void?",
        ques : "The speed of light is 300,000 kilometres per second. It is true in a peticular space since res = ",
        anw : " x-> . <-x = x-> . 0 . <-x = x-x",
        rep : 0
    },

    {
        groups : "Memory",
        hint : "Superstiscious x-x = 2",
        ques : "Some say I am the luckiest one, but others not thus got 2 things. Who am I?",
        anw : "So i am the opposite of unlucky then somewhat end with number 2",
        rep : 2
    },

    {
        groups : "Memory",
        hint : "bow and bough -> equals ? ",
        ques : "Have you ever tried to tie a bough with a bow and arrow?, Classical, find the problemn in this sentence ? ASCII difference in number",
        anw : "Sperated by the ASCII maybe like images?",
        rep : 256
    },

    {
        groups : "Memory",
        hint : "Maybe you don't need to thinks this much",
        ques : "If today is the 5th english class so nextweek will be ...",
        anw : "So this question was nothing, isn't it?",
        rep : 6
    },

    {
        groups : "Memory",
        hint : "jj/mm/years is false but still need an number",
        ques : "Where was the first english classroom ?",
        anw : "After this end, (I suppose you are plans)",
        rep : 191
    }

];

//hard to understand T.T
var weird_question = [
    {
        groups : "Enigma",
        hint : "Players refers to one team as non-players doesn't plays, so they must have something with one team and even put the foot on the grass, then just use the rules",
        ques : "How many non-players players are on a regulation-sized football (soccer) field?",
        anw : "So there is two group, separated but together in a peticular way",
        rep : 7
    },
    {
        groups : "Enigma",
        hint : "They are important for me, you see ?",
        ques : "7 one sick one sleeping but 5 friend with 6 sages",
        anw : "So there is two group, separated but together in a peticular way",
        rep : 5
    },
    {
        groups : "Grammar",
        hint : " (void) words + meaning (void) + 1",
        ques : "How many differences you may find between     : the use of 'affect' and 'effect'?",
        anw : "It begin by (void) however need a beginning but since there is suite, not end by (void)",
        rep : 14
    },
    {
        groups : "Grammar",
        hint : "I have been playing at Fortnite .... power.",
        ques : "J’ai joué à Fortnite quand ma mère a coupé la lumière, give me english traduction then you got the number +19 -1",
        anw : "Lack of luck maybe a special day?",
        rep : 31
    },

    {
        groups : "Grammar",
        hint : " 'not' and 'continuous' (P,PP,P,PC,F,FC+PP,PPC,PP,PPC,FP,FPC) + ambiguous",
        ques : "Clasical number of voices for an human forEach(prime => {;})",
        anw : 19,
        rep : 19
    },
];

var code = "70130312" ;

